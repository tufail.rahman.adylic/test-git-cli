import { subtract } from '../index';

describe('@test/add', () => {
  it('Should add numbers', () => {
    const total = subtract(10, 5);
    expect(total).toBe(5);
  });
});
