import { add } from '../index';

describe('@test/add', () => {
  it('Should add numbers', () => {
    const total = add(5, 5);
    expect(total).toBe(10);
  });
});
