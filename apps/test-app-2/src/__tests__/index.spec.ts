import testApp from '../index';

describe('@testing-git-ci-cd/test-app', () => {
  it('substract', () => {
    const results = testApp(10, 5);
    expect(results).toBe(5);
  });
});
