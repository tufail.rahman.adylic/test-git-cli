import { subtract } from '@testing-git-ci-cd/subtract';

export default (a: number, b: number) => {
  return subtract(a, b);
};
